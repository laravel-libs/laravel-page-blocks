<?php

namespace Pilyavskiy\PB\Model;

use Illuminate\Database\Eloquent\Model;

abstract class PBModel extends Model
{
    const VIEW = 'default';
    const VIEW_VARIABLE = 'var';

    public $timestamps = false;

    public function pageBlock()
    {
        return $this->morphOne('Pilyavskiy\PB\Model\PageBlock', 'blockable');
    }

    public function getView(): string
    {
        return static::VIEW;
    }

    public function getVariable(): string
    {
        return static::VIEW_VARIABLE;
    }
}
