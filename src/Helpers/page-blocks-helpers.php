<?php

if (!function_exists('uploads_asset')) {
    function uploads_asset(string $path = '')
    {
        return asset('uploads/' . $path);
    }
}

if (!function_exists('clean_phone')) {
    function clean_phone(string $phone = '')
    {
        return str_replace([' ', '-', '/'], '', $phone);
    }
}
