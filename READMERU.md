## Laravel Page Blocks

Мы создали этот пакет для динамического создания веб-страниц, с разными и изменяемыми блоками.

### Установка пакета
Для установки пакета вам нужно:
```bash
composer require pilyavskiy/laravel-page-blocks
```

**Это требуется только в том случае, если вы используете Laravel 5.4!**
Для включения в проект, пожалуйста, добавьте PBServiceProvider в свой `config/app.php` файл:
```php
Pilyavskiy\PB\PBServiceProvider::class
```

Затем запустите миграцию для создания структуры блоков и страниц:
```bash
php artisan migrate
```
Для вас будут созданы таблицы: pages и page_blocks

### Структура таблицы страниц (pages):
- id - auto increment
- page - string, unique названия страницы
- route - string, unique маршрут страницы, по значению из этого поля вы будете иметь доступ к вашей странице.*
- title - string, заголовок вашей страницы
- metaTitle - string, uses for html tag `<title>`
- metaDescription - text, uses for html tag `<description>`
- metaKeywords - text, uses for html tag `<keywords>`
- isModified - boolean, возможность изменения страницы
- isDeletable - boolean, возможность удаления страницы

### Структура таблицы блоков (page_blocks):
- id - auto increment
- page_id - integer, id страницы к которой относииться данный блок
- blockable_type - string, namespace для блока*
- blockable_id - integer, id блока, что указан в blockable_type
- sortingOrder - integer, используется для сортировки блоков на одной странице

**Важно: для страницы индекса используйте `/` в качестве маршрута**
**Пример - App\Models\PageBlock\BannerBlock**

### Создания новой страницы
Для создания страниц мы ииспользуем seeds (если это новый проект), или migrations, если нужно добавить страницы готовый проект.
```php
$page = Page::create(
    [
        'page'            => 'index',
        'route'           => '/',
        'title'           => 'Index',
        'metaTitle'       => 'Meta title example',
        'metaDescription' => 'Meta description example',
        'metaKeywords' => 'Meta keywords example',
        'isModified' => true,
        'isDeletable' => false
    ]
);
```
**Это пример создания index (main) страницы в вашем проекте**

```php
$page = Page::create(
    [
        'page'            => 'contact-us',
        'route'           => 'contact-us',
        'title'           => 'Contact us',
        'metaTitle'       => 'Meta title example',
        'metaDescription' => 'Meta description example',
        'metaKeywords' => 'Meta keywords example',
        'isModified' => true,
        'isDeletable' => false
    ]
);
```
**Это пример создания contact us страницы в вашем проекте**

### Создания блоков 
После включения пакета вам будет доступна команда для создания моделей для новых блоков для страниц:
```bash
php artisan make:pbmodel PageBlockExample
```

Для создания нового блока страницы с миграцией (migration) и представлением (view), пожалуйста, добавьте флаг `-b` (рекомендовано)
```bash
php artisan make:pbmodel PageBlockExample -b
```
После этого у вас будет создано: модель, миграция и представления.

#### Модель (model)
По умолчанию, все блоки страниц будут добавлятся в ваш проект в папку `app/Models/PageBlocks`, вы можете изменить этот путь для всех новых блоков в файле конфигурации `config/laravel-page-blocks` в `pbmodel.namespace`

В этой модели блока страницы вы уже найдете константы `VIEW` и `VIEW_VARIABLE`. `VIEW` - это путь к представлению(view) этого конкретного блока (по правилам Laravel Blade), по необходимости вы можете его изменять. `VIEW_VARIABLE` - это переменая в которой будет находится вся информация из базы для этого конкретного блока в его представлении (view), по необходимости вы можете его изменять, но мы вам этого не рекомендуем делать, так как значения этой переменой уже находится в представление (view) блока, как подсказка.

**Мы рекомендуем добавить в `$fillable` все поля что вы добавили или планируете добавить для этого блока**

```php
class ExampleBlockModel extends PBModel
{

    protected $fillable = ['title', 'description', 'image'];

    /**
    * It's name of view for you block type
    */
    const VIEW = 'blocks.page.block';

    /**
    * It's variable name what you can use in your view
    * All data in this variable will be from your model
    */
    const VIEW_VARIABLE = 'exapmleVariable';
}
```

#### Миграция (migration)
Создайте нужную для вас структуру блока, включите все изменяемые части. Все даные которые будут в базе для этого блока будут доступны для использования в представление (путь к которому находится в констанке `VIEW`). Все данные для конкретного блока будут находится в переменой `VIEW_VARIABLE`.

Эти миграции не имеют никаких отличий от базовых мииграций Laravel.


#### Представления (view)

Это место где у будет находится ваша верстка. Все представления в формате blade. 



### Routes
Маршруты блоков страниц всегда являются последними маршрутами, зарегистрированными вашим приложением.
Этот пакет подерживает только пути (routes) первого уровня (/services, /contact-us), если вам нужны более сложные пути (routes), вы можете зарегистрировать их отдельно в файле роутов (обычно это `routes/web.php`). Для использования блоков страниц в таком случае вас нужно вернуть екземпляр модели Page с вашего контроллера, пример 

```php
public function showRegistrationForm(PageRepository $pageRepository)
{
    $slug = config('laravel-page-blocks.pages.login');
    $page = $pageRepository->read($slug);

    if (empty($page)) {
        abort(404);
    }

    return view('laravel-page-blocks::default', [
        'page' => $page
    ]);
}
```
Для систематиизации таких путей (routes), мы рекомендуем использувать массив pages в `config/laravel-page-blocks.php`, где ключ - удобное для вас обозначения страницы, а значения должно соответствовать значению в колонке `route` таблицы `pages`.

Если вам не нужно дублировать роуты, к примеру вы используете путь (route) 'blog', и перенесли его как описано выше по новому пути (route) 'customer/blog', и хотите чтобы путь (route) 'blog' больше не использовался, в таком случае вы можете исключить его из автоматических путей в массиве exclude в `config/laravel-page-blocks.php`.

### Publish
Для публикации представлений (views) и миграции (migrations) из пакета, пожалуйста, запустите
```bash
 php artisan vendor:publish
``` 
 Выбрать `Provider: Pilyavskiy\PB\PBServiceProvider`.

### Views
Файл `resources/views/vendor/laravel-page-blocks/default.blade.php` используется для показа страницы с блоками страниц, этот файл нужно возращать в случае работы с блоками страниц в своем контроллере.

Файл `resources/views/vendor/laravel-page-blocks/template.blade.php` это файл стартовой разметки страницы, вы можете редактировать ее для ваших нужд. Обычно это добавления стилей, скриптов и т.п.

Файл `resources/views/vendor/laravel-page-blocks/content.blade.php`, это файл для перебора и показа блоков страниц.
Вы можете сключить файл `laravel-page-blocks::content`, внутрь ваших страниц, но вы должны быть уверены что передали екземпляр модели Page в переменую `$page`.

